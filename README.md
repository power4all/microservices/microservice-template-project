# Template Instructions
use this template when creating a new microservice. It contains the basic setup of EF with mysql, route and folder naming conventions.

## Steps

- Rename SLN
- Rename API Project
- Rename Test Project 
- Rename Namespace
- Rename Root namespace
- Rename DbContext
- Rename project file and .dll names inside Dockerfile: 

<pre>
FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build
WORKDIR /src
<b>COPY EnergyDataCollectionService.API.csproj . </b>
RUN dotnet restore
COPY . .
RUN dotnet publish -c release -o /app

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app .
<b>ENTRYPOINT ["dotnet", "EnergyDataCollectionService.API.dll"]</b>
</pre>
