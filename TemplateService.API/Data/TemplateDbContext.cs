﻿using System;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EnergyDataCollectionService.Data
{
	public partial class TemplateDbContext : DbContext
	{
		public TemplateDbContext(DbContextOptions<TemplateDbContext> options)
			: base(options)
		{
		}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}


		partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
	}
}